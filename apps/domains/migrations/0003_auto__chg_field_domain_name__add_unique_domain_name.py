# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Domain.name'
        db.alter_column(u'domains_domain', 'name', self.gf('libs.fields.DomainNameField')(unique=True, max_length=255))
        # Adding unique constraint on 'Domain', fields ['name']
        db.create_unique(u'domains_domain', ['name'])


    def backwards(self, orm):
        # Removing unique constraint on 'Domain', fields ['name']
        db.delete_unique(u'domains_domain', ['name'])


        # Changing field 'Domain.name'
        db.alter_column(u'domains_domain', 'name', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'domains.dns': {
            'Meta': {'object_name': 'DNS'},
            'dns_type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mx_server': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'sub_domain': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'domains.domain': {
            'Meta': {'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['domains']