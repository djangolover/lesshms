# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Domain.is_root'
        db.add_column(u'domains_domain', 'is_root',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Domain.is_root'
        db.delete_column(u'domains_domain', 'is_root')


    models = {
        u'domains.dns': {
            'Meta': {'object_name': 'DNS'},
            'dns_type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domain': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['domains.Domain']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mx_server': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'sub_domain': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'})
        },
        u'domains.domain': {
            'Meta': {'ordering': "['name']", 'object_name': 'Domain'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_root': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('libs.fields.DomainNameField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['domains']