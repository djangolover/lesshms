# -*- coding: utf-8 -*-
from django.conf.urls import *

urlpatterns = patterns('apps.domains.views',
    url(regex=r'^$', view='domain_list', name='all'),
    url(regex=r'^add/$', view='domain_ac', name='add_domain'),
    url(regex=r'^(?P<domain_id>\d+)/$', view='domain_ac', name='change_domain'),
    url(regex=r'^(?P<domain_id>\d+)/delete/$', view='domain_delete', name='delete_domain'),
    url(regex=r'^(?P<domain_id>\d+)/dns/$', view='domain_dns', name='dns'),
    url(regex=r'^(?P<domain_id>\d+)/dns/(?P<dns_id>\d+)/$', view='domain_dns_ac', name='change_dns'),
    url(regex=r'^(?P<domain_id>\d+)/dns/add/$', view='domain_dns_ac', name='add_dns'),
    url(regex=r'^(?P<domain_id>\d+)/dns/(?P<dns_id>\d+)/delete/$', view='domain_dns_delete', name='delete_dns'),

)