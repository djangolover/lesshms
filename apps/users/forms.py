# -*- coding: utf-8 -*-

from django import forms

from apps.users.models import LinuxGroup


class NewUserForm(forms.Form):
    group = forms.ModelChoiceField(
        label=u"Group",
        required=False,
        queryset=LinuxGroup.objects.all(),
        empty_label=u"------",
        widget=forms.Select(attrs={
            "class": "form-control",
        })
    )

    create_group = forms.BooleanField(
        label=u"Create new group",
        required=False,

    )

    group_name = forms.CharField(
        label=u"New group name",
        required=False,
        max_length=32,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "disabled": "disabled",
            "placeholder": "For example, lesshms"
        }),
        help_text=u"Recommend is the same as the username."
    )

    username = forms.CharField(
        label=u"Username",
        required=True,
        max_length=32,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "For example, lesshms"
        }),
        help_text=u"The short and simple name to log on SSH."
    )

    password = forms.CharField(
        label=u"Password",
        required=True,
        max_length=32,
        widget=forms.TextInput(attrs={
            "class": "form-control"
        }),
        help_text=u"A strong password."
    )

