# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'LinuxGroup'
        db.create_table(u'users_linuxgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'users', ['LinuxGroup'])

        # Adding model 'LinuxUser'
        db.create_table(u'users_linuxuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('default_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.LinuxGroup'])),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'users', ['LinuxUser'])


    def backwards(self, orm):
        # Deleting model 'LinuxGroup'
        db.delete_table(u'users_linuxgroup')

        # Deleting model 'LinuxUser'
        db.delete_table(u'users_linuxuser')


    models = {
        u'users.linuxgroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'LinuxGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.linuxuser': {
            'Meta': {'ordering': "['username']", 'object_name': 'LinuxUser'},
            'default_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.LinuxGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['users']