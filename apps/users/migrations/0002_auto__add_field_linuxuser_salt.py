# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'LinuxUser.salt'
        db.add_column(u'users_linuxuser', 'salt',
                      self.gf('django.db.models.fields.CharField')(default='salt33', max_length=6),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'LinuxUser.salt'
        db.delete_column(u'users_linuxuser', 'salt')


    models = {
        u'users.linuxgroup': {
            'Meta': {'ordering': "['name']", 'object_name': 'LinuxGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.linuxuser': {
            'Meta': {'ordering': "['username']", 'object_name': 'LinuxUser'},
            'default_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.LinuxGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'salt': ('django.db.models.fields.CharField', [], {'default': "'salt33'", 'max_length': '6'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['users']