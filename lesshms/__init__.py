
PROJECT_DIRECTORY_ABC = "0123456789abcdefghijklmnopqrstuvwxyz_"

# VERSIONS
DJANGO_VERSIONS = [
    "1.5.4", "1.5.3", "1.5.2", "1.5.1", "1.5",
    "1.4.8", "1.4.7", "1.4.6", "1.4.5", "1.4.4", "1.4.3",
    "1.3.7", "1.3.6", "1.3.5",
    "1.2.7",
    "1.1.4",
    "1.0.4",
]

SOUTH_VERSIONS = [
    "0.8.2"
]

PIL_VERSIONS = [
    "1.1.6"
]

# uwsgi --ini /home/lesshms/uwsgi/lesshms_test.ini
# /etc/init.d/nginx restart