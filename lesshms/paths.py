# -*- coding: utf-8 -*-
import os
from django.conf import settings

if settings.DEVELOP:
    ROOT = "/var/www/lesshms_dev"
else:
    ROOT = "/"

ETC = os.path.join(ROOT, "etc")
INIT_D = os.path.join(ETC, "init.d")

#
# Bind
#

BIND = os.path.join(ETC, "bind")
BIND_NAMED_CONF = os.path.join(BIND, "named.conf")
BIND_NAMED_CONF_LOCAL = os.path.join(BIND, "named.conf.local")


#
# Nginx
#

NGINX = os.path.join(ETC, "nginx")
NGINX_SE = os.path.join(NGINX, "sites-enabled")
NGINX_UWSGI = os.path.join(NGINX, "uwsgi_params")


#
#  Virtualevn
#

# Path to virtualenv
if settings.DEVELOP:
    # Mac OS X
    VIRTUAL_ENV = "/usr/local/bin/virtualenv"
else:
    # Debian
    VIRTUAL_ENV = "/usr/bin/virtualenv"


#
# Home
#
HOME = os.path.join(ROOT, "home")

# TODO: need real username
USERNAME = "nikita"

# UWSGI
UWSGI_PATH = "unix:///var/run/uwsgi/"

#
# System
#

CMD_USERADD = "/usr/sbin/useradd"
CMD_GROUPADD = "/usr/sbin/groupadd"