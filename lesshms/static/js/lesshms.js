var TIMES = {
    "create_project": 3,
    "create_user": 3,
    "uwsgi": 35,
    "virtualenv": 3,
    "django": 35,
    "south": 7,
    "pil": 10,
    "nginx": 4
};

var unocc = "An unexpected error has occurred.";
var time_left = 0;

var packages = {
    "uwsgi": true,
    "django": false,
    "south": false,
    "pil": false
};


var errorCallbacks = {
    404: function() {
        show_error("Page not found. WTF?");
    },
    500: function() {
        show_error(unocc);
    }
};

function set_time_left() {
    if (time_left < 0) {
        time_left = 0;
    }
    $("#processModal .time-left").html("Time left " + time_left + " sec");
    time_left = time_left - 1;
}

function start_time(sec) {
    time_left = sec;
    setInterval(set_time_left, 1000);
}

function show_error(error) {
    $.ajaxq.clear('createProject');
    set_progress_bar(100);

    $("#processModal .progress").addClass("progress-bar-danger");
    $("#processModal .progress").removeClass("active");
    $("#processModal .alert").show();
    $("#processModal .alert").html(error);
}

function set_progress_bar(value) {

    if (value == 0) {
        $("#processModal .progress").removeClass("progress-bar-danger");
        $("#processModal .progress").addClass("active");
        $("#processModal .alert").hide();
    }

    if (value == 100) {
        time_left = 0;
        $("#progress-close").removeClass("disabled");

        if (REDIRECT_TO == 'project') {
            window.location.href="/projects/" + $("#project_id").html() + "/";
        }

        if (REDIRECT_TO == 'packages') {
            window.location.href="/projects/" + $("#project_id").html() + "/packages/";
        }
    }

    $("#processModal .progress-bar").attr({
        "aria-valuenow": value,
        "style":  "width: " + value + "%;"
    });
}

function set_progress_title(title) {
    $("#processModal .progress-title").html(title);
}


function create_virtualenv(project_id) {
    $.ajaxq('createProject', {
        url: "/projects/subprocess/create_virtualenv/",
        type: "POST",
        dataType: "json",
        data: {
            "project_id": project_id
        },
        beforeSend: function(jqXHR, settings) {
            set_progress_title("Create virtualenv");
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                jqXHR.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            }
        },
        success: function(data) {
            if (data["status"] == "success") {
                set_progress_bar(9);
            } else if (data["status"] == "error") {
                show_error(data["error"])
            } else {
                show_error(unocc);
            }
        },
        error: function(data) {
            show_error(data);
        },
        statusCode: errorCallbacks
    });
}



function install_packages(project_id, package_name, package_version, only_package) {

    $.ajaxq('createProject', {
        url: "/projects/subprocess/install_packages/",
        type: "POST",
        dataType: "json",
        data: {
            project_id: project_id,
            package_name: package_name,
            package_version: package_version
        },
        beforeSend: function(jqXHR, settings) {
            set_progress_title("Install " + package_name + " " + package_version);
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                jqXHR.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            }
        },
        success: function(data) {
            if (only_package) {
                set_progress_bar(100);
            } else {
                var percent = 80;
                if (package_name == "uwsgi") percent = 44;
                if (package_name == "django") percent = 79;
                if (package_name == "south") percent = 86;
                if (package_name == "pil") percent = 96;
                set_progress_bar(percent);
            }

        },
        statusCode: errorCallbacks
    });
}

function create_user(project_id, username, password) {

    $.ajaxq('createProject', {
        url: "/projects/subprocess/create_user/",
        type: "POST",
        dataType: "json",
        data: {
            project_id: project_id,
            username: username,
            password: password
        },
        beforeSend: function(jqXHR, settings) {
            set_progress_title("Create user " + username);
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                jqXHR.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            }
        },
        success: function(data) {
            set_progress_bar(6);
        },
        statusCode: errorCallbacks
    });
}

function setup_nginx(project_id) {

    $.ajaxq('createProject', {
        url: "/projects/subprocess/setup_nginx/",
        type: "POST",
        dataType: "json",
        data: {
            project_id: project_id
        },
        beforeSend: function(jqXHR, settings) {
            set_progress_title("Setup nginx");
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                jqXHR.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            }
        },
        success: function(data) {
            set_progress_bar(100);
        },
        statusCode: errorCallbacks
    });
}


function create_project(directory, name, domain_id, user_id) {

    $.ajaxq('createProject', {
        url: "/projects/add/",
        type: "POST",
        dataType: "json",
        data: {
            directory: directory,
            name: name,
            domain_id: domain_id,
            user_id: user_id
        },
        beforeSend: function(jqXHR, settings) {
            set_progress_bar(0);
            set_progress_title("Create project");
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                jqXHR.setRequestHeader("X-CSRFToken", getCookie('csrftoken'))
            }
        },
        success: function(data) {
            set_progress_bar(3);
            var project_id = data["id"];
            $("#project_id").html(project_id);

            // Created new_user
            if (!data['user_id']) {
                var username = $("#id_username").val();
                var password = $("#id_password").val()
                create_user(project_id, username, password)
            }

            create_virtualenv(project_id);

            // Install packages
            $.each(packages, function(key, value) {
                if (value) {
                    install_packages(project_id, key, value, false);
                }
            });

            // Nginx
            setup_nginx(project_id);
        },
        statusCode: errorCallbacks
    })
}


$(document).ready(function () {
    $("#save_project").click(function () {

        var directory = $("#id_directory").val();
        var name = $("#id_name").val();
        var domain_id = $("#id_domain").val();

        var time = 0;

        $('#processModal').modal({
            "backdrop": false
        });

        // Project
        if (directory && name && domain_id) {
            time += TIMES["create_project"];
            time += TIMES["virtualenv"];

            // Packages
            $.each(packages, function(key, value) {
                var version = $("#id_" + key + "_version").val();
                if (version) {
                    packages[key] = version;
                    time += TIMES[key];
                }

                if (key == "uwsgi") {
                    packages[key] = "1.9.17";
                    time += TIMES["uwsgi"];
                }
            });

            var user_id = '';
            if (!$("#id_create_user").is(":checked")) {
                user_id = $("#id_user").val();
            } else {
                time += TIMES["create_user"];
            }

            start_time(time);

            create_project(directory, name, domain_id, user_id);
        }

        return false;
    });

    $("#add_package_buttom").click(function () {
        $('#addPackageModal').modal('show');
        return false;
    })

    $("#add_package").click(function () {
        $('#processModal').modal({
            "backdrop": false
        });

        var project_id = $("#project_id").html();
        var package_name = $("#id_package").val();
        var package_version = $("#id_version").val();

        install_packages(project_id, package_name, package_version, true);

        return false;
    })

    $("#id_create_group").click(function () {
        var checked = $("#id_create_group").is(":checked");
        if (checked) {
            $("#id_group").attr("disabled", "disabled");
            $("#id_group_name").removeAttr("disabled");
        } else {
            $("#id_group").removeAttr("disabled");
            $("#id_group_name").attr("disabled", "disabled");
        }
    });

    $("#id_create_user").click(function () {
        var checked = $("#id_create_user").is(":checked");
        if (checked) {
            $("#id_user").attr("disabled", "disabled");
            $("#id_username").removeAttr("disabled");
            $("#id_password").removeAttr("disabled");
        } else {
            $("#id_user").removeAttr("disabled");
            $("#id_username").attr("disabled", "disabled");
            $("#id_password").attr("disabled", "disabled");
        }
    })
})