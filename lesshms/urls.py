from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from libs.bootstrap.forms import BootstrapAuthenticationForm


urlpatterns = patterns('',
    url(r'^$', 'apps.dashboard.views.dashboard', name='dashboard'),
)

# Auth
urlpatterns += patterns('django.contrib.auth.views', url(
    view='login',
    regex=r'^login/$',
    kwargs={
        'template_name': 'registration/login.html',
        'authentication_form': BootstrapAuthenticationForm
    },
    name='login')
)

urlpatterns += patterns('django.contrib.auth.views', url(
    view='logout',
    regex=r'^logout/$',
    kwargs={'next_page': '/'},
    name='logout'
)
)

# Apps
urlpatterns += patterns('',
    (r'^domains/', include('apps.domains.urls', namespace="domains")),
    (r'^projects/', include('apps.projects.urls', namespace="projects")),
    (r'^users/', include('apps.users.urls', namespace="users")),
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )
