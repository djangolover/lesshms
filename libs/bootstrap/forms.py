# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import AuthenticationForm


class BootstrapAuthenticationForm(AuthenticationForm):
    username = forms.CharField(
        label=u"Username",
        max_length=75,
        widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": u"Username"
        }),
    )

    password = forms.CharField(
        label=u"Пароль",
        widget=forms.PasswordInput(attrs={
            "class": "form-control",
            "placeholder": u"Password"
        })
    )

    def clean_username(self):
        return self.cleaned_data.get('username', '').lower()